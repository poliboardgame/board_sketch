#include <MeetAndroid.h>

//Biblioteca para bluetooth
MeetAndroid meetAndroid;

//Pino de dado do sensor rfid
int leitorRfid = 5;

void setup()  
{
  // baudrate do dongle bluetooth 
  Serial.begin(57600); 
 
  //inicializacao do pino do rfid como saida
  pinMode(leitorRfid, INPUT);
}

void loop()
{
  // Nao receberemos eventos do celular, portanto esta chamada nao e necessaria
  //meetAndroid.receive(); 
  
  // le o pino do rfid e envia para o android
  meetAndroid.send(analogRead(sensor));
  
  // tempo para nao manter o celular muito ocupado
  // No futuro, criar uma rotina como:
  // Enviar um dado de uma leitura apenas quando ele for diferente do lido anteriormente 
  
  delay(100);
}


